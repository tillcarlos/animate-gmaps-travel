const util = require('util');

var str = '!3m1!4b1!4m13!4m12!1m5!1m1!1s0x47a84e373f035901:0x42120465b5e3b70!2m2!1d13.404954!2d52.5200066!1m5!1m1!1s0x479e75f9a38c5fd9:0x10cb84a7db1987d!2m2!1d11.5819806!2d48.1351253';
// var str = '!3m1!4b1!4m20!4m19!1m5!1m1!1s0x1e955fe737bd22e5:0xf5b813675e007ba8!2m2!1d28.3023579!2d-25.7297871!1m5!1m1!1s0x1e955e5c875906fd:0xa65176214cdebc80!2m2!1d28.3374283!2d-25.7657075!1m5!1m1!1s0x1e9560c06dba5b73:0x57122f60632be1a1!2m2!1d28.274954!2d-25.7832822!3e0';

var parts = str.split('!').filter(function(s) { return s.length > 0; }),
	root = [],						// Root elemet
	curr = root,					// Current array element being appended to
	m_stack = [root,],				// Stack of "m" elements
	m_count = [parts.length,];	// Number of elements to put under each level

console.log(parts);

var m_countdown = m_count[m_count.length - 1];
parts.forEach(function(el) {
	var kind = el.substr(1, 1),
		value = el.substr(2);
	console.log(kind + ' - ' + value);

	// Decrement all the m_counts
	for (var i = 0; i < m_count.length; i++) {
		m_count[i]--;
	}

	if (kind === 'm') {
		var new_arr = [];
		m_count.push(value);
		curr.push(new_arr);
		m_stack.push(new_arr);
		curr = new_arr;
	}
	else {
		if (kind == 'b') {
			curr.push(value == '1');
		}
		else if (kind == 'd' || kind == 'f') {
			curr.push(parseFloat(value));
		}
		else if (kind == 'i' || kind == 'u' || kind == 'e') {
			curr.push(parseInt(value));
		}
		else {
			curr.push(value);
		}
	}

	while (m_count[m_count.length - 1] === 0) {	// Next value shouldn't go onto array
		m_stack.pop();
		m_count.pop();
		curr = m_stack[m_stack.length - 1];
		console.log("Arr complete");
	}
});

console.log(util.inspect(root, { depth: null }));