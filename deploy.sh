#!/bin/bash

DIR=$(dirname $0)
HTPROOT="/var/www/TODO/html"
OFFLINEPATH="$DIR"

WEBPATH="$HTPROOT"
SSH_SERVER="TODO"
SSH_USER="TODO"

EXCLUDE=" --exclude .git --exclude meta --exclude asset-sources --exclude wp-config.php  --exclude node_modules --exclude logs"

#gulp --production

#rsync -avz --delete $OFFLINEPATH/ $SSH_USER@$SSH_SERVER:$WEBPATH
rsync -avz $EXCLUDE --delete $OFFLINEPATH $SSH_USER@$SSH_SERVER:$WEBPATH

ssh $SSH_USER@$SSH_SERVER "chown -R www-data.www-data $HTPROOT"

#scp $OFFLINEPATH/lib/config_production.php $SSH_USER@$SSH_SERVER:$WEBPATH/lib/config.php

#ls dist/styles | grep 'main-.*' | xargs -I {} scp $OFFLINEPATH/dist/styles/{}  #$SSH_USER@$SSH_SERVER:$WEBPATH/templates/head_inline_css.php

echo "Deploy finished"






